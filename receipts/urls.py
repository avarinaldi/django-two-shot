from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
    ExpenseCategoryCreateView,
    AccountCreateView,
)

urlpatterns = [
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("", ReceiptListView.as_view(), name="home"),
]
